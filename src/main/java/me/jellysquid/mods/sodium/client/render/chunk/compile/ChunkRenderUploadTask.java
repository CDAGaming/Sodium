package me.jellysquid.mods.sodium.client.render.chunk.compile;

public abstract class ChunkRenderUploadTask {
    public abstract void performUpload();
}
